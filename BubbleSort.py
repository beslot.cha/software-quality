read = open("Bubbles.txt", 'r')
content = read.read()

def mySplit(string):
    strings = []
    current_string = ""
    for char in string:
        if char == " ":
            strings.append(current_string)
            current_string = ""
        else:
            current_string += char
    strings.append(current_string)
    return strings


def swapPositions(table, pos1, pos2): 
    table[pos1], table[pos2] = table[pos2], table[pos1] 
    return table


def Order(table): 
    tableLength = len(table)
    index = 0
    for index in range(tableLength):
        for index in range(0, tableLength-index-1):
            if int(table[index]) > int(table[index+1]):
                swapPositions(table, index, index+1)
    return table


table = mySplit(content)
print(table)

table_final = Order(table)
print(table_final)

    