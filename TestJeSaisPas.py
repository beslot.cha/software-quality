import unittest


def myFct(x):
    return "demerdez vous"


class TestMyLister(unittest.TestCase):

    def test_basic(self):
        self.assertEqual(myFct("((15 ÷ (7 − (1 + 1))) × 3) − (2 + (1 + 1))"), "15 7 1 1 + − ÷ 3 × 2 1 1 + + −")

    def test_autre(self):
        self.assertEqual(myFct("((1 + 2) × 4) + 3"), "1 2 + 4 × 3 +")

    def test_string(self):
        self.assertIsInstance(myFct("((15 ÷ (7 − (1 + 1))) × 3) − (2 + (1 + 1))"), str)

    def test_zero(self):
        with self.assertRaises(ValueError):
            myFct("15 / 0")

    def test_operateur(self):
        with self.assertRaises(ValueError):
            myFct("14 + * 2")


if __name__ == '__main__':
    unittest.main()