import unittest

def myTest4(param1, param2):
    return "test4"


class TestStringMethods(unittest.TestCase):

    def testAllInputs(self):
        input1 = None
        input2 = None
        self.assertEqual(myTest4(input1,input2), 'Enter the required parameters' )
    

    def testValueInput(self):
        input1 = None
        input2 = 64
        self.assertEqual(myTest4(input1,input2), 'Enter a entry value' )


    def testBaseInput(self):
        input1 = "sapintro bi1" 12
        input2 = None
        self.assertEqual(myTest4(input1,input2), 'Enter a base' )


    def testInput1MaxSize(self):
        input1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis nisl feugiat, tempor sem et, interdum diam. Suspendisse eget molestie massa. Vivamus nec tempor orci, ut finibus augue. Duis at consectetur mauris, a fringilla nisi. Nunc magna nisl, pellentesque in posuere eu, iaculis nec justo. Nunc elementum sapien ut rhoncus efficitur. Sed eu libero luctus, faucibus est eget, suscipit diam. Etiam quis molestie orci. Aliquam pellentesque nunc augue, at accumsan ex hendrerit nec. Cras ac euismod libero. In aliquam metus et metus pulvinar pulvinar. Nullam tempor. "
        input2 = 64
        self.assertEqual(myTest4(input1,input2), 'Value is too big' )

    
    def testInput1MinSize(self):
        input1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales sollicitudin turpis et sagittis. Integer in porta nibh. Maecenas iaculis mattis arcu, in. "
        input2 = 64
        self.assertEqual(myTest4(input1, input2), 'Value is too little compared to base' )


    def testInput2MinSize(self):
        input1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis nisl feugiat, tempor sem et, interdum diam. Suspendisse eget molestie massa. Vivamus nec tempor orci, ut finibus augue. Duis at consectetur mauris, a fringilla nisi. Nunc magna nisl, pellentesque in posuere eu, iaculis nec justo. Nunc elementum sapien ut rhoncus efficitur. Sed eu libero luctus, faucibus est eget, suscipit diam. Etiam quis molestie orci. Aliquam pellentesque nunc augue, at accumsan ex hendrerit nec. Cras ac euismod libero. In aliquam metus et metus pulvinar pulvinar. Nullam tempor. "
        input2 = 64
        self.assertEqual(myTest4(input1,input2), 'Base is too little compared to value' )

    
    def testInput2MaxSize(self):
        input1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sodales sollicitudin turpis et sagittis. Integer in porta nibh. Maecenas iaculis mattis arcu, in. "
        input2 = 64
        self.assertEqual(myTest4(input1, input2), 'Base is too big compared to value' )


    def testBaseMaxSize(self):
        input1 = "drzerzesqdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"
        input2 = 258
        self.assertEqual(myTest4(input1, input2), 'Base is too big' )


    def testBaseMinSize(self):
        input1 = "drzerzesqdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"
        input2 = 1
        self.assertEqual(myTest4(input1, input2), 'Base is too little' )


    def testValueMaxSize(self):
        input1 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nisi sapien, porta vitae feugiihezuhfgyutqgfigiyFSUDFHhFHoiOoiGFHOgrihgourhqguihrughmsuhgqphgquhgiurhgiurqhiughqiuat at, condimentum et orci. Vestibulum quis consectetur est. Morbi imperdiet tellus vulputate tempor ullamcorper. Fusce ultrices aliquam dui eget elementum. Phasellus risus magna, euismod non enim id, interdum tristique eros. Donec volutpat, tellus ac pharetra molestie, enim nibh rutrum augue, pretium rutrum magna velit quis mauris. Cras varius aliquam lacus ut interdum. Ut lobortis eros dolor, in hendrerit eros ultricies ut. Nunc erat dui, laoreet eu elit eu, mollis cursus sapien.Quisque hendrerit lectus blandit, aliquam orci eu, mollis orci. Suspendisse non enim eleifend, lacinia felis et, dapibus tortor. Nulla eleifend tellus in facilisis venenatis. Nulla leo erat, varius vitae ornare eu, lacinia vel erat. Quisque ornare bibendum risus ut lacinia. Vestibulum ultrices in augue sit amet lobortis. Donec eu sollicitudin arcu, vitae tempus orci. Integer viverra molestie neque, a feugiat ligula maximus id. Proin bibendum in orci quis elementum. Curabitur blandit interdum ligula, et auctor ligula gravida vitae. In hac habitasse platea dictumst. Curabitur erat velit, luctus nec magna ac, aliquam porttitor quam. Integer sodales consectetur ex quis varius. Cras massa lorem, sollicitudin eu lorem sit amet, pellentesque tempor nibh. Aliquam porta metus a est convallis imperdiet.Mauris blandit eros nec quam cursus, sed euismod ipsum mattis. Mauris vel nisi nulla. Nulla placerat vitae ligula quis scelerisque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse ut eros dictum, varius sapien in, scelerisque mi. Proin ante nisi, tincidunt sit amet blandit et, tempor nec lectus. Nullam sit amet. "
        input2  = 64
        self.assertEqual(myTest4(input1, input2), 'Value is too big' )


    def testTypeInputBase(self):
        input1 = ('zaEAzea' 12 @'546')
        input2 = 16
        self.assertEqual(myTest4(input1, input2), 'Unexpected character : Uppercase strings are not allowed in the chosen base' )


    def testTypeInputBase(self):
        input1 = 'yessa'
        input2 = 5
        self.assertEqual(myTest4(input1,input2), 'Chosen base does not exist')


if __name__ == '__main__':
    unittest.main()    