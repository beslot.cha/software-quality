import unittest

def myRound(floatNumber, separatorNumber):
    print("arrondis")

class TestStringMethods(unittest.TestCase):


    def testMultipleSeparator(self):
        input = 8.36476
        self.assertEqual(myRound(input, 2), 8.36 )

    
    def testTypeSeparator(self):
        separator = 2.4554
        self.assertTrue(type(seprator), int)


    def testInput(self):
        input = None
        self.assertEqual(myRound(input), 'Enter the required parameter' )


    def testInputMaxSize(self):
        input = float('inf') + 1
        self.assertEqual(myRound(input), 'Number is too big' )

    
    def testInputMinSize(self):
        input = float('-inf') - 2
        self.assertEqual(myRound(input), 'Number is too little' )


    def testTypeInput(self):
        input = 8
        self.assertEqual(myRound(input), input )


    def testTypeInput(self):
        input = 8.3
        self.assertTrue(type(input),int)
    

    def testTypeOutput(self):
        input = 8.3
        self.assertTrue(type(myRound(input)),int)


if __name__ == '__main__':
    unittest.main()    