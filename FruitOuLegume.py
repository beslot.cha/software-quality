import unittest

def fruitLegume(string):
    fruit = ["fraise","clémentine","pomme","poire","abricot"]
    legume = ["tomate","carotte","patate","haricot vert","comcombre"]

    return 'la categorie'

class testFruitOuLegume(unittest.TestCase):
    def test_input(self):
        s = 'patate'
        self.assertTrue(type(s),str)
        self.assertEqual(fruitLegume(""),"mangez quelque chose d'équilibré")

    def test_existe(self):
        self.assertFalse(fruitLegume("salade"),"salade existe pas")

    def test_exact(self):
        self.assertEqual(fruitLegume("haricot"),"légume")

    def test_accent(self):
        self.assertEqual(fruitLegume("clementine"),"fruit")

if __name__ == '__main__':
    unittest.main()