read = open("Bubbles.txt", 'r')
content = read.read()

def mySplit(string):
    strings = []
    current_string = ""
    for char in string:
        if char == " ":
            strings.append(current_string)
            current_string = ""
        else:
            current_string += char
    strings.append(current_string)
    return strings


def fastOrder(table, left, right):
    pivot = table[(left+right)//2]
    i = left
    j = right
    while True:
        while table[i] < pivot:
            i += 1
        while table[j] > pivot:
            j -= 1
        if i > j:
            break
        if i < j:
            table[i], table[j] = table[j], table[i]
        i+=1
        j-=1
    if left < j:
        fastOrder(table,left,j)
    if i < right:
        fastOrder(table,i,right)
    return table


table = mySplit(content)
print(table)
print('\n')
final_table = fastOrder(table, 0, len(table)-1)
print(final_table)