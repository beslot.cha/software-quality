import unittest

def mySplit(string, letter):
    if string == None or string != str or letter == None or letter != str  :
        return "Enter the good value to your parameter"
    stri = str(string)
    strv = ''
    for w in stri:
        if w != letter:
            strv += w
    return strv

class SplitTest(unittest.TestCase):

    def test_myfonction_simple(self):
        self.assertEqual(mySplit("c'est du caca", 'a'), "c'est du cc")

    def test_myfonction_chiffre(self):
        self.assertEqual(len(mySplit("Helloworld", 'o')), 8)

    def test_myfonction_str(self):
        self.assertIsInstance(mySplit("n'importe quoi", "o"), str)

    def test_entier(self):
        self.assertEqual(mySplit(8178100716168706, "4,5"), "8178100716168706")

if __name__ == '__main__':
    unittest.main()
