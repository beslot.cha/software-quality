def intFunction(string):

    newInt = 0

    for number in string :
        intString = ord(number)-48
        newInt = newInt*10 + intString
    return newInt


print(intFunction('183732763'))
print(type(intFunction('183732763')))


def stringFunction(integer):

    integerNew = ""

    while integer != 0:
        integerChar = chr(integer % 10 + 48)
        integerNew = integerNew + integerChar
        integer = int(integer/10)
    return integerNew[::-1]
        

print(stringFunction(183732763))
print(type(stringFunction(183732763)))